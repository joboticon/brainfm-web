# Unofficial Brain.fm Webapp

Ubuntu Touch Webapp for Brain.fm (webapp-container for the brain.fm website)

Use UT Tweak Tool to prevent app suspension so that playback will continue when the screen is off.

## License

Copyright (C) 2020  Joe Liau

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
